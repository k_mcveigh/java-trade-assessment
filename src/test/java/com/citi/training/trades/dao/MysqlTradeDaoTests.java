package com.citi.training.trades.dao;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {

    @Autowired
    MysqlTradeDao mysqlTradeDao;

    @Test
    @Transactional
    public void test_createAndFindAll() {
        mysqlTradeDao.create(new Trade(-1, "AAPL", 100.0,2000));

        assertEquals(1, mysqlTradeDao.findAll().size());
    }

    @Test
    @Transactional
    public void test_createAndFindOne() {
        mysqlTradeDao.create(new Trade(-1, "AAPL", 100.0,2000));

        assertNotNull(mysqlTradeDao.findById(3));
    }

    @Test(expected = TradeNotFoundException.class)
    @Transactional
    public void test_createAndDelete() {
        mysqlTradeDao.create(new Trade(-1, "AAPL", 100.0,2000));
        assertNotNull(mysqlTradeDao.findById(3));
        mysqlTradeDao.deleteById(3);
        mysqlTradeDao.findById(3);
    }

    @Test(expected = TradeNotFoundException.class)
    @Transactional
    public void test_createAndFindById_Fail() {
        mysqlTradeDao.create(new Trade(-1, "AAPL", 100.0,2000));
        mysqlTradeDao.findById(3);
    }

}
