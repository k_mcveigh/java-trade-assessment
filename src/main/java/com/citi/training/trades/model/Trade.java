package com.citi.training.trades.model;

public class Trade {

    private int id;
    private String stock;
    private Double price;
    private int volume;

    public Trade() {
    }

    public Trade(int id, String stock, Double price, int volume) {
        this.id = id;
        this.stock = stock;
        this.price = price;
        this.volume = volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "id=" + id +
                ", stock='" + stock + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                '}';
    }
}
