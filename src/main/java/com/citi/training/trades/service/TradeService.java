package com.citi.training.trades.service;

import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.model.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TradeService {

    @Autowired
    private TradeDao tradeDao;

    public List<Trade> findAll(){
        return tradeDao.findAll();
    }

    public Trade findById(int id){
        return tradeDao.findById(id);
    }

    public Trade create(Trade trade){
        return tradeDao.create(trade);
    }

    public void deleteById(int id){
        tradeDao.deleteById(id);
    }
}
