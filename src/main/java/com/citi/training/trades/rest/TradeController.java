package com.citi.training.trades.rest;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.service.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST Interface class for {@link com.citi.training.trades.model.Trade} object.
 * @see Trade
 *
 */

@RestController
@RequestMapping("/trades")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
 
    @Autowired
    private TradeService tradeService;

    /**
     * Find all {@link com.citi.training.trades.model.Trade}s in the database.
     * @return {@link com.citi.training.trades.model.Trade}s that was found
     */
    @RequestMapping(method= RequestMethod.GET,
                    produces= MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> findAll(){
        LOG.debug("findAll() was called");
        return tradeService.findAll();
    }

    /**
     * Find an {@link com.citi.training.trades.model.Trade} by it's integer id.
     * @param id The id of the trade
     * @return {@link com.citi.training.trades.model.Trade} that was found
     * @throws com.citi.training.trades.exceptions.TradeNotFoundException if the trade is not found
     */
    @RequestMapping(value="/{id}", method= RequestMethod.GET,
                    produces= MediaType.APPLICATION_JSON_VALUE)
    public Trade findById(@PathVariable int id) {
        LOG.debug("findById() was called, id: " + id);
        return tradeService.findById(id);
    }

    /**
     * Create {@link com.citi.training.trades.model.Trade}.
     * @param trade A trade object
     * @return {@link com.citi.training.trades.model.Trade} that was created
     *
     */
    @RequestMapping(method= RequestMethod.POST,
                    consumes= MediaType.APPLICATION_JSON_VALUE,
                    produces= MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trade> create(@RequestBody Trade trade) {
        LOG.debug("create was called, trade: " + trade);
        return new ResponseEntity<Trade>(tradeService.create(trade),
                                            HttpStatus.CREATED);
    }

    /**
     * Delete a {@link com.citi.training.trades.model.Trade} by it's integer id.
     * @param id The id of the trade
     * @throws com.citi.training.trades.exceptions.TradeNotFoundException if the trade is not found
     */
    @RequestMapping(value="/{id}", method= RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody
    void deleteById(@PathVariable int id) {
        LOG.debug("deleteById was called, id: " + id);
        tradeService.deleteById(id);
    }
}
