package com.citi.training.trades.dao;

import com.citi.training.trades.model.Trade;
import java.util.List;

public interface TradeDao {

    List<Trade> findAll();

    Trade findById(int id);

    Trade create(Trade trade);

    void deleteById(int id);
}
