package com.citi.training.trades.exceptions;

public class TradeNotFoundException extends RuntimeException {

    public TradeNotFoundException(String msg) {
        super(msg);
    }
}
